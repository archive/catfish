from setuptools import setup

setup(
    name="catfish",
    version="0.0.0",
    url="https://github.com/realorangeone/catfish",
    license="MIT",
    author="Jake Howard",
    description="Catfish",
    packages=["catfish"],
    include_package_data=True,
    zip_safe=False,
    pathon_requires=">=3.7",
    install_requires=[
        "click",
        "daemonize>=2.5.0",
        "psutil",
        "aiohttp",
        "ujson",
        "pyzmq",
        "aiofiles",
        "python-dotenv",
        "prettytable",
    ],
    entry_points="""
        [console_scripts]
        ctf=catfish.__main__:cli
    """,
    project_urls={"GitHub: Issues": "https://github.com/realorangeone/catfish/issues"},
)
