import os
import shlex
import subprocess

import click
from prettytable import PrettyTable

from catfish.utils.sockets import socket_has_data

from . import Project


@click.group()
def project():
    pass


@project.command()
@click.argument("command", nargs=-1, type=str)
@click.pass_context
def run(ctx, command):
    command = " ".join(command)
    project = Project(os.getcwd())
    proc = subprocess.Popen(
        shlex.split(command),
        env={**os.environ, **project.get_environment()},
        stdout=subprocess.PIPE,
    )
    while proc.poll() is None:
        while socket_has_data(proc.stdout):
            line = proc.stdout.readline()
            if not line:
                break
            click.echo(line, nl=False)
    return ctx.exit(proc.returncode)


@project.command()
@click.option("--running-only", is_flag=True, default=False)
def status(running_only):
    project = Project(os.getcwd())
    table = PrettyTable(["ident", "pid", "port"])
    for process in project.processes:
        if running_only and not process.is_running:
            continue
        table.add_row(
            [
                click.style(process.ident, fg=process.colour),
                process.pid or "",
                process.port or "",
            ]
        )
    click.echo(table.get_string(sortby="ident"))
