import asyncio
import time

import psutil

from catfish.router import start_router
from catfish.utils.processes import terminate_processes
from catfish.utils.sockets import BASE_SOCKET_DIR

from .server import WORKER_SERVER_SOCKET, start_server

PID_FILE = BASE_SOCKET_DIR.joinpath("catfish.pid")


def is_running():
    if not PID_FILE.exists():
        return False
    if not WORKER_SERVER_SOCKET.exists():
        return False
    try:
        get_running_process()
    except ValueError:
        return False
    return True


def get_running_process() -> psutil.Process:
    with open(PID_FILE) as f:
        return psutil.Process(int(f.read()))


def wait_for_running_worker():
    while not is_running():
        time.sleep(0.1)


def stop_worker():
    if is_running():
        terminate_processes([get_running_process()])


async def run_worker(port):
    loop = asyncio.get_running_loop()
    await asyncio.gather(start_router(loop, port), start_server())


def run(port):
    return asyncio.run(run_worker(port))
