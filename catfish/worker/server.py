import asyncio
import atexit
import os
import shlex
import signal
import socket
import subprocess
from enum import Enum, auto
from typing import Optional

import click
import zmq
from aiohttp.test_utils import unused_port

import ujson
from catfish.project import Process, Project
from catfish.utils import aio
from catfish.utils.processes import CURRENT_PROCESS, terminate_subprocesses
from catfish.utils.sockets import BASE_SOCKET_DIR, NEW_LINE, read_all_from_socket

WORKER_SERVER_SOCKET = BASE_SOCKET_DIR.joinpath("catfish.sock")


class PayloadType(Enum):
    PING = auto()
    PROCESS = auto()


def send_to_server(type: PayloadType, payload):
    with socket.socket(socket.AF_UNIX, type=socket.SOCK_STREAM) as sock:
        sock.connect(str(WORKER_SERVER_SOCKET))
        sock.sendall(
            ujson.dumps({"type": type.value, "payload": payload}).encode() + NEW_LINE
        )
        return read_all_from_socket(sock)


def read_logs_for_process(process: Process):
    ctx = zmq.Context()
    sock = ctx.socket(zmq.SUB)
    socket_path = str(BASE_SOCKET_DIR.joinpath(process.logs_socket))
    sock.connect("ipc://" + str(socket_path))
    sock.setsockopt_string(zmq.SUBSCRIBE, "")
    while True:
        yield sock.recv_string().strip()


def write_data(writer, data):
    writer.write(ujson.dumps(data).encode())


async def publish_stdout_for(
    process, ctf_process: Process, project: Project, port: Optional[int]
):
    ctx = zmq.Context()
    sock = ctx.socket(zmq.PUB)
    socket_path = str(BASE_SOCKET_DIR.joinpath(ctf_process.logs_socket))
    sock.bind("ipc://" + socket_path)
    try:
        while True:
            while True:
                output = await process.stdout.readline()
                if not output:
                    break
                sock.send_string(output.decode())
            await process.wait()
            exit_code = process.returncode
            if exit_code in [-signal.SIGHUP, 0, 1]:
                # If process gets SIGHUP, or exits cleanly / uncleanly, restart it
                process = await start_process(project, ctf_process, port)
    finally:
        sock.close()
        ctx.destroy()
        await aio.remove_file(socket_path)


async def start_process(project: Project, process: Process, port: Optional[int]):
    process_env = {
        **os.environ,
        **project.get_environment(),
        "CATFISH_IDENT": process.ident,
        "CATFISH_WORKER_PROCESS": str(CURRENT_PROCESS.pid),
    }
    command = process.command
    if port is not None:
        process_env["PORT"] = str(port)
        command = command.replace("$PORT", str(port))
    return await asyncio.create_subprocess_exec(
        *shlex.split(command),
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        env=process_env,
        cwd=project.root
    )


async def run_process_command(project: Project, process: Process):
    if "$PORT" in process.command:
        port = unused_port()
    else:
        port = None
    proc = await start_process(project, process, port)
    asyncio.ensure_future(publish_stdout_for(proc, process, project, port))
    return proc


async def parse_payload(payload):
    data = ujson.loads(payload)
    return PayloadType(data["type"]), data["payload"]


async def client_connected(reader, writer):
    payload_type, data = await parse_payload(await reader.readline())
    if payload_type == PayloadType.PROCESS:
        project = Project(data["path"])
        process = project.get_process(data["process"])
        proc = await run_process_command(project, process)
        write_data(writer, {"pid": proc.pid})
    elif payload_type == PayloadType.PING:
        write_data(writer, {"ping": "pong"})
    else:
        write_data(writer, {"error": "Invalid command"})
    await writer.drain()
    writer.close()


async def start_server():
    atexit.register(terminate_subprocesses)
    server = await asyncio.start_unix_server(client_connected, WORKER_SERVER_SOCKET)
    click.echo("Started server")
    await server.serve_forever()
