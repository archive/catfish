import pkg_resources

__version__ = pkg_resources.require("catfish")[0].version
