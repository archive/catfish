import functools

import click
import daemonize

from catfish import __version__, worker
from catfish.project.cli import project as project_cli
from catfish.utils.sockets import create_base_socket_dir, delete_base_socket_dir


@click.group()
@click.version_option(__version__, prog_name="catfish")
def cli():
    pass


@cli.command()
@click.option("--port", default=8080, type=int)
@click.option("--no-fork", is_flag=True)
@click.pass_context
def start(ctx, port, no_fork):
    if worker.is_running():
        ctx.fail("Worker already running")

    create_base_socket_dir()

    if no_fork:
        return worker.run(port)
    daemon = daemonize.Daemonize(
        "catfish", worker.PID_FILE, functools.partial(worker.run, port), verbose=True
    )
    try:
        daemon.start()
    except SystemExit:
        pass
    worker.wait_for_running_worker()
    proc = worker.get_running_process()
    click.echo("Worker started with pid {}".format(proc.pid))


@cli.command()
@click.pass_context
def stop(ctx):
    if not worker.is_running():
        ctx.fail("Worker not running")
    proc = worker.get_running_process()
    click.echo("Terminating process {}".format(proc.pid))
    worker.stop_worker()
    delete_base_socket_dir()


cli.add_command(project_cli)


if __name__ == "__main__":
    cli()
