import click
from aiohttp import web

from .utils import get_hostname_from_request


async def handle_request(request):
    return web.json_response({"host": get_hostname_from_request(request)})


def get_server():
    application = web.Application()
    application.router.add_route("*", r"/{path:.*}", handle_request)
    return application


async def start_router(loop, port):
    click.echo("Starting router...")
    aiohttp_server = get_server()
    aio_server = await loop.create_server(aiohttp_server, "0.0.0.0", port)
    click.echo("Router listening on port {}".format(port))
    await aio_server.serve_forever()
