from typing import Optional

import psutil

from catfish.utils.processes import get_root_process

ROOT_PROCESS = get_root_process()
HOSTNAME_ENV_VAR = "VIRTUAL_HOST"


def get_process_for_hostname(hostname: str) -> Optional[psutil.Process]:
    for process in ROOT_PROCESS.children(recursive=True):
        try:
            if hostname in process.environ().get(HOSTNAME_ENV_VAR, "").split(","):
                return process
        except psutil.AccessDenied:
            continue
    return None


def get_hostname_from_request(request) -> str:
    if getattr(request, "url", None) is not None:
        return request.url.host
    return request.host.split(":")[0]
