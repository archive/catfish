import os
import select
import shutil
import tempfile
from pathlib import Path

import ujson

BASE_SOCKET_DIR = Path(tempfile.gettempdir()).joinpath("catfish")

BUFFER_SIZE = 4096
DEFAULT_SOCKET_READ_TIMEOUT = 0.01
NEW_LINE = b"\n"


def socket_has_data(socket, timeout=DEFAULT_SOCKET_READ_TIMEOUT) -> bool:
    readable, _, _ = select.select([socket], [], [], timeout)
    return socket in readable


def read_all_from_socket(socket):
    data = b""
    while NEW_LINE not in data:
        message = socket.recv(BUFFER_SIZE)
        if message == b"":
            break
        data += message
    return ujson.loads(data)


def create_base_socket_dir():
    try:
        os.mkdir(BASE_SOCKET_DIR)
    except FileExistsError:
        pass


def delete_base_socket_dir():
    try:
        shutil.rmtree(BASE_SOCKET_DIR)
    except FileNotFoundError:
        pass
