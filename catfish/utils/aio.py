import asyncio
import os

from aiofiles import os as aios

remove_file = aios.wrap(os.remove)
path_exists = aios.wrap(os.path.exists)


async def await_file_exists(path: str):
    while True:
        exists = await path_exists(path)
        if exists:
            return
        await asyncio.sleep(0.1)
