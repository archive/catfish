#!/usr/bin/env python3

import time

print("Started")  # noqa: T001

time.sleep(1)

print("Ended")  # noqa: T001

exit(1)
