#!/usr/bin/env python3

import time
from itertools import count


for num in count():
    time.sleep(0.5)
    print("Round {}".format(num))  # noqa: T001
