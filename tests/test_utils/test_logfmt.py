from catfish.utils.logfmt import logfmt
from tests import BaseTestCase


class LogFmtTestCase(BaseTestCase):
    def test_simple_logfmt(self):
        self.assertEqual(
            logfmt({"key1": "value1", "key2": "value2"}), "key1=value1 key2=value2"
        )

    def test_quotes_space(self):
        self.assertEqual(
            logfmt({"key1": "value1 valuea", "key2": "value2 valueb"}),
            'key1="value1 valuea" key2="value2 valueb"',
        )

    def test_quotes_equals(self):
        self.assertEqual(
            logfmt({"key1": "value1=valuea", "key2": "value2=valueb"}),
            'key1="value1=valuea" key2="value2=valueb"',
        )

    def test_quotes_quotes(self):
        self.assertEqual(
            logfmt({"key1": '"value1"', "key2": '"value2"'}),
            'key1="\\"value1\\"" key2="\\"value2\\""',
        )
        self.assertEqual(
            logfmt({"key1": "'value1'", "key2": "'value2'"}),
            "key1=\"'value1'\" key2=\"'value2'\"",
        )

    def test_empty(self):
        self.assertEqual(logfmt({}), "")

    def test_boolean_values(self):
        self.assertEqual(logfmt({"key1": True, "key2": False}), "key1=true key2=false")

    def test_none_values(self):
        self.assertEqual(logfmt({"key1": None, "key2": None}), "key1= key2=")

    def test_numbers(self):
        self.assertEqual(
            logfmt({"key1": 12345, "key2": -12345}), "key1=12345 key2=-12345"
        )
        self.assertEqual(
            logfmt({"key1": "12345", "key2": "-12345"}), "key1=12345 key2=-12345"
        )

    def test_float(self):
        self.assertEqual(
            logfmt({"key1": 12345.67, "key2": -12345.67}),
            "key1=12345.67 key2=-12345.67",
        )
        self.assertEqual(
            logfmt({"key1": "12345.67", "key2": "-12345.67"}),
            "key1=12345.67 key2=-12345.67",
        )
