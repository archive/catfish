import os
import subprocess

from aiohttp.test_utils import make_mocked_request

from catfish.router import utils
from tests import BaseTestCase


class ProcessForHostnameTestCase(BaseTestCase):
    def test_no_matching_processes(self):
        self.assertIsNone(utils.get_process_for_hostname("localhost"))

    def test_finds_process(self):
        proc = subprocess.Popen(
            str(self.DUMMY_EXE),
            stdout=subprocess.PIPE,
            env={**os.environ, utils.HOSTNAME_ENV_VAR: "localhost"},
        )
        self.assertIsNone(proc.poll())
        self.assertEqual(utils.get_process_for_hostname("localhost").pid, proc.pid)

    def test_finds_process_with_multiple_hostnames(self):
        proc = subprocess.Popen(
            str(self.DUMMY_EXE),
            stdout=subprocess.PIPE,
            env={**os.environ, utils.HOSTNAME_ENV_VAR: "localhost,test.local"},
        )
        self.assertIsNone(proc.poll())
        self.assertEqual(utils.get_process_for_hostname("localhost").pid, proc.pid)
        self.assertEqual(utils.get_process_for_hostname("test.local").pid, proc.pid)


class HostnameFromRequestTestCase(BaseTestCase):
    def get_request_for_hostname(self, hostname):
        return make_mocked_request("GET", "/", headers={"HOST": hostname})

    def test_hostname(self):
        self.assertEqual(
            utils.get_hostname_from_request(self.get_request_for_hostname("localhost")),
            "localhost",
        )

    def test_hostname_with_port(self):
        self.assertEqual(
            utils.get_hostname_from_request(
                self.get_request_for_hostname("localhost:8080")
            ),
            "localhost",
        )
