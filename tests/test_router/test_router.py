from tests import BaseRouterTestCase


class RouterTestCase(BaseRouterTestCase):
    async def test_accessible(self):
        response = await self.client.request("GET", "/")
        self.assertEqual(response.status, 200)
