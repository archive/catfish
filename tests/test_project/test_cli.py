from io import StringIO

from dotenv import dotenv_values

from catfish.project import Project
from tests import BaseTestCase


class ProjectRunCLITestCase(BaseTestCase):
    def setUp(self):
        super().setUp()
        self.project = Project(self.EXAMPLE_DIR)

    def test_sets_environment(self):
        with self.in_example_dir():
            result = self.run_cli(["project", "run", "env"], catch_exceptions=False)
        self.assertEqual(result.exit_code, 0)
        env = dotenv_values(stream=StringIO(result.stdout))
        self.assertEqual(env["FOO"], "bar")
        self.assertEqual(env["PYTHONUNBUFFERED"], "1")
        self.assertIsNone(env.get("CATFISH_IDENT"))
        self.assertIn(str(self.project.root), env["PATH"])
        for path in self.project.get_extra_path():
            self.assertIn(str(path), env["PATH"])

    def test_sets_working_dir(self):
        with self.in_example_dir():
            result = self.run_cli(["project", "run", "pwd"], catch_exceptions=False)
        self.assertEqual(result.exit_code, 0)
        self.assertEqual(result.stdout.strip(), str(self.project.root))


class ProjectStatusCLITestCase(BaseTestCase):
    def setUp(self):
        super().setUp()
        self.project = Project(self.EXAMPLE_DIR)

    def test_shows_all_processes(self):
        with self.in_example_dir():
            result = self.run_cli(["project", "status"])
        self.assertEqual(result.exit_code, 0)
        output = result.stdout
        for process in self.project.processes:
            self.assertIn(process.ident, output)

    def test_only_shows_running_processes(self):
        with self.in_example_dir():
            result = self.run_cli(["project", "status", "--running-only"])
        self.assertEqual(result.exit_code, 0)
        output = result.stdout
        for process in self.project.processes:
            self.assertNotIn(process.ident, output)
