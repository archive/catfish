#!/usr/bin/env bash

set -e

export PATH=env/bin:${PATH}

black setup.py catfish tests
isort -rc setup.py catfish tests
