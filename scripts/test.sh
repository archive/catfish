#!/usr/bin/env bash

set -e

export PATH=env/bin:${PATH}

echo "> Running tests..."
nose2 $@ -C --coverage catfish --verbose --coverage-report term --coverage-report html

echo "> Running formatter..."
black catfish tests setup.py --check

echo "> Running linter..."
flake8 catfish tests setup.py --ignore=E128,E501

echo "> Running isort..."
isort -rc -c catfish tests setup.py

echo "> Running type checker..."
mypy --strict-optional --ignore-missing-imports catfish
